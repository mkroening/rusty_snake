use std::collections::VecDeque;
use rand::{thread_rng, Rng};
use sdl2::pixels::Color;

use super::snack::Snack;

pub struct Snake {
    positions: VecDeque<(i8, i8)>,
    grid_size: (i8, i8),
  }

const BODY: Color = Color {
    r: 200,
    g: 0,
    b: 100,
    a: 0xff,
};

impl Snake {
    pub fn random(grid_size: (i8, i8)) -> Snake {
        Snake {
            positions: VecDeque::from(vec![(
                thread_rng().gen_range(0, grid_size.0),
                thread_rng().gen_range(0, grid_size.1),
            )]),
            grid_size: grid_size,
        }
    }

    pub fn do_move(&mut self, direction: (i8, i8), snack: &mut Snack) -> Result<(), ()> {
        let new_front = (
            (self.positions.front().unwrap().0 + direction.0 + self.grid_size.0) % self.grid_size.0,
            (self.positions.front().unwrap().1 + direction.1 + self.grid_size.1) % self.grid_size.1,
        );

        if self.positions.len() >= 2 && self.positions[1] == new_front {
            return self.do_move((-direction.0, -direction.1), snack);
        } else if self.positions.contains(&new_front) {
            return Err(());
        }
        
        self.positions.push_front(new_front);

        if self.positions.front().unwrap() != &snack.position {
            self.positions.pop_back();
        } else {
            *snack = Snack::random(self.grid_size);
        }

        Ok(())
    }

    pub fn draw_on(&self, drawer: &mut dyn FnMut((i8, i8), Color)) {
        for indices in &self.positions {
            drawer(*indices, BODY);
        }
    }
}
