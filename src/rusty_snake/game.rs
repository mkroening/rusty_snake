use sdl2::{pixels::Color, rect::Rect, render::Canvas, video::Window, EventPump};
use std::{time, thread};

use super::{snack::Snack, snake::Snake};

pub struct Game {
  grid_size: (i8, i8),
  rect_size: (u32, u32),
  canvas: Canvas<Window>,
  events: EventPump,
  snake: Snake,
  snack: Snack,
}

const BACKGROUND: Color = Color {
  r: 0x23,
  g: 0x0f,
  b: 0x0d,
  a: 0xff,
};

impl Game {
  pub fn new(grid_size: (i8, i8), rect_size: (u32, u32)) -> Game {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let (column_count, row_count) = grid_size;
    let (rect_width, rect_height) = rect_size;
    let (canvas_width, canvas_height) = (
      rect_width * column_count as u32,
      rect_height * row_count as u32,
    );
    let window = video_subsystem
      .window("Rusty Snake", canvas_width, canvas_height)
      .position_centered()
      .build()
      .unwrap();

    let mut game = Game {
      grid_size: grid_size,
      rect_size: rect_size,
      canvas: window.into_canvas().present_vsync().build().unwrap(),
      events: sdl_context.event_pump().unwrap(),
      snake: Snake::random(grid_size),
      snack: Snack::random(grid_size),
    };

    game.clear();
    game.canvas.present();

    game
  }

  pub fn run(&mut self) {
    let mut direction = (1, 0);
    loop {
      for event in self.events.poll_iter() {
        use sdl2::event::Event;
        use sdl2::keyboard::Keycode;
        direction = match event {
          Event::KeyDown {
            keycode: Some(Keycode::Up),
            ..
          } => (0, -1),
          Event::KeyDown {
            keycode: Some(Keycode::Down),
            ..
          } => (0, 1),
          Event::KeyDown {
            keycode: Some(Keycode::Left),
            ..
          } => (-1, 0),
          Event::KeyDown {
            keycode: Some(Keycode::Right),
            ..
          } => (1, 0),
          Event::Quit { .. }
          | Event::KeyDown {
            keycode: Some(Keycode::Escape),
            ..
          } => return,
          _ => continue,
        }
      }
      match self.snake.do_move(direction, &mut self.snack) {
        Err(()) => self.snake = Snake::random(self.grid_size),
        Ok(()) => (),
      }
      self.clear();
      let canvas = &mut self.canvas;
      let rect_size = &self.rect_size;
      let mut drawer = |indices: (i8, i8), color: Color| {
        canvas.set_draw_color(color);
        canvas
          .fill_rect(Rect::new(
            rect_size.0 as i32 * indices.0 as i32,
            rect_size.1 as i32 * indices.1 as i32,
            rect_size.0,
            rect_size.1,
          ))
          .unwrap();
      };
      self.snake.draw_on(&mut drawer);
      self.snack.draw_on(&mut drawer);
      self.canvas.present();
      thread::sleep(time::Duration::from_millis(200));
    }
  }

  fn clear(&mut self) {
    self.canvas.set_draw_color(BACKGROUND);
    self.canvas.clear();
  }
}
