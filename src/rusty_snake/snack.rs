use rand::{thread_rng, Rng};
use sdl2::pixels::Color;

pub struct Snack {
  pub position: (i8, i8)
}

const COLOR: Color = Color {
    r: 100,
    g: 200,
    b: 0,
    a: 0xff,
};

impl Snack {
    pub fn random(grid_size: (i8, i8)) -> Snack {
        Snack {
            position: (
                thread_rng().gen_range(0, grid_size.0),
                thread_rng().gen_range(0, grid_size.1)
            )
        }
    }

    pub fn draw_on(&self, drawer: &mut dyn FnMut((i8, i8), Color)) {
        drawer(self.position, COLOR);
    }
}
