use rusty_snake::game::Game;

mod rusty_snake;

fn main() {
    let mut game = Game::new((10, 10), (72, 72));
    game.run();
}
